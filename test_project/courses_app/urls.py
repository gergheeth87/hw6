from django.urls import path
from .views import check_auth_page

urlpatterns = [
    path("check_auth_page", check_auth_page),
]
