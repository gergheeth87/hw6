from django.shortcuts import render


def check_auth_page(request):
    return render(request, 'courses_app/check_auth_page.html')
