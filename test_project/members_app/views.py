from django.shortcuts import render
from .models import UserInput
from .forms import UserInputForm
from django.shortcuts import redirect
import requests


def index_page(request):
    return render(request, "members_app/index_page.html")


def input_page(request):
    if request.method == "POST":
        form = UserInputForm(request.POST)
        if form.is_valid():
            user_input = form.save(commit=False)
            user_input.save()
            return redirect("http://127.0.0.1:8000/display_page")
    else:
        form = UserInputForm()
    return render(request, "members_app/input_page.html", {'form': form})


def display_page(request):
    posts = UserInput.objects.all()
    return render(request, "members_app/display_page.html", {'posts': posts})


def request_page(request):
    response = requests.get("http://127.0.0.1:8000/display_page")
    return render(request, "members_app/request_result_page.html", {'response': response})
