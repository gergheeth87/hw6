from django.db import models


class UserInput(models.Model):
    objects = None
    user_input = models.CharField(max_length=50)

    def publish(self):
        self.save()

    def __str__(self):
        return self.user_input


class UserWithAnimal(models.Model):
    user_name = models.CharField(max_length=15)


class AnimalType(models.Model):
    animal_type = models.CharField(max_length=20)
    user_with_animal = models.OneToOneField(UserWithAnimal, on_delete=models.CASCADE)
