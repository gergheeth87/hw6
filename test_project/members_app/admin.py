from django.contrib import admin
from .models import (
    UserWithAnimal,
    AnimalType
)

admin.site.register(UserWithAnimal)
admin.site.register(AnimalType)
