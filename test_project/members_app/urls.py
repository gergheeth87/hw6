from django.urls import path
from .views import input_page, display_page, index_page, request_page

urlpatterns = [
    path("", index_page),
    path("input_page", input_page),
    path("display_page", display_page),
    path("request_page", request_page),
]
