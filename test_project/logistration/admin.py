from django.contrib import admin
from .models import CustomUser, UserProfile


class UserProfileInline(admin.StackedInline):
    model = UserProfile


class CustomUserAdmin(admin.ModelAdmin):
    model = CustomUser
    inlines = [UserProfileInline]


admin.site.register(CustomUser, CustomUserAdmin)
