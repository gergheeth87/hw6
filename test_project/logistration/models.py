from django.db import models
from django.contrib.auth.models import AbstractUser


class CustomUser(AbstractUser):
    job = models.CharField(max_length=50, blank=True, null=True)
    hobby = models.CharField(max_length=100, blank=True, null=True)


class UserProfile(models.Model):
    favorite_animal = models.CharField(max_length=100, null=True, blank=True)
    favorite_color = models.CharField(max_length=10, null=True, blank=True)
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE)
